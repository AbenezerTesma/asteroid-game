# Astreroid Game

### Download Source code  

~~~
git clone https://bitbucket.org/AbenezerTesma/asteroid-game
~~~

### Open Source code  

~~~
Open ATesmaFinalProject.sln in Visual Studio
~~~

### To build or rebuild a single project  

```
In Visual Studio then Solution Explorer, choose the project.
On the menu bar, choose Build, and then choose either Build ProjectName or Rebuild ProjectName.

```

### About LISENCE

Copyright [2019] [Abenezer Tesma]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
I prefer Aoache License, version 2.0 because It's free to use and there are no many ristrictions like other licensing company. 