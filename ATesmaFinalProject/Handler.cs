﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//includind the pakage
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace ATesmaFinalProject
{
    class Handler
    {
        //list for all enemies calling it enemies 
        public List<enemie> enemies = new List<enemie>();
        //Giving the handler time double
        public double timer = 2D;
        //variable to set the time to the timer
        public double maxtime = 2D;
        // making a filed that increase the speed of the enemie
        public int afterspeed = 240;
        //counter for the game timmer
        public float timecounter = 0;


        //creating bool to determine the main menu or not
        public bool Ongame = false;
        public void HandlerUpdate(GameTime Time)
        {
            //if true the game is is still running if false then main menu 
            if (Ongame)
            {
                timer -= Time.ElapsedGameTime.TotalSeconds;
                //counting the time up 
                timecounter += (float)Time.ElapsedGameTime.TotalSeconds;
            }
            //if the user is not on menu 
            else
            {
                // testing what button is pressed
                KeyboardState K = Keyboard.GetState();
                if (K.IsKeyDown(Keys.Space))
                {
                    Ongame = true;
                    timecounter = 0;
                    timer = 2;
                    maxtime = 2;
                    afterspeed = 240;
                }
                //or the user can use right click from mouse to start the game
                if (Mouse.GetState().LeftButton == ButtonState.Pressed)
                {
                    Ongame = true;
                    timecounter = 0;
                }


            }

            //if the time is 0 the reset again
            if (timer <= 0)
            {
                enemies.Add(new enemie(afterspeed));
                //timer = 2D;
                timer = maxtime;
                // if the maxtime is > 0.5 and the next time the timer hit 0 set the timer to be 1.9 and 1.8, 1.7  
                if (maxtime > 0.5)
                {
                    maxtime -= 0.1d;
                }
                //if the speed of the enemie is < 720 then add speed by 4
                if (afterspeed < 720)
                {
                    afterspeed += 4;
                }


            }
        }
    }
}
