﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
namespace ATesmaFinalProject
{
    public class ActionScene : GameScene
    {
       

        //private SpriteBatch spriteBatch;
        //private Bat bat;
       // GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        //Abenezer Starts
        Texture2D Ship_sprite;
        Texture2D enemi_Sprite;
        Texture2D space_sprite;
        //for font
        SpriteFont gameFont;
        SpriteFont timerFont;
        //creating object refering player to the screen 
        Ships player = new Ships();

        //creating instance for enemie
        //enemie testenemie = new enemie(250);
        //creating instance for handeler
        Handler handler = new Handler();
        public Game game;

        public ActionScene(Game game,
            SpriteBatch spriteBatch) : base(game)
        {
            

            //this.spriteBatch = spriteBatch;
            //Texture2D tex = game.Content.Load<Texture2D>("Images/Bat");
            //bat = new Bat(game, spriteBatch, tex);
            //this.Components.Add(bat);
            // Create a new SpriteBatch, which can be used to draw textures.
            //spriteBatch = new SpriteBatch(GraphicsDevice);
            this.spriteBatch = spriteBatch;
            
            this.game = Game;


            Ship_sprite = game.Content.Load<Texture2D>("Images/ship");
            enemi_Sprite =game.Content.Load<Texture2D>("Images/Ufo");
            space_sprite = game.Content.Load<Texture2D>("Images/Background");

            //font

            gameFont = game.Content.Load<SpriteFont>("Fonts/spaceFont");
            timerFont = game.Content.Load<SpriteFont>("Fonts/timerFont");

           

            Song backsong = this.game.Content.Load<Song>("Sound/SpaceshipSound");
            MediaPlayer.IsRepeating = true;
            MediaPlayer.Play(backsong);


        }

        public override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            //Abenezer strat
            spriteBatch.Begin();
            spriteBatch.Draw(space_sprite, new Vector2(0, 0), Color.White);
            //Drawing the ship image to the topleft corner
            spriteBatch.Draw(Ship_sprite, new Vector2(player.position.X - 34, player.position.Y - 50), Color.White);
            //Draw Menu Text if ongame bool is false
            if (handler.Ongame == false)
            {
                //storing message in to MenuMessage
                string MessageStart = "Press 'Spacebar' To Start";
                string MessageOr = "OR";
                string MessageEsc = "Press 'ESC' To Main Menu";
                string MessageScore = "High Score: " + Math.Floor(handler.timecounter).ToString() + "  Seconds ";
              

                //geting the width and hight of the Text by passing the MenuMessage
                Vector2 Text = gameFont.MeasureString(MessageStart);

               
                //making the Text in center
                spriteBatch.DrawString(gameFont, MessageStart, new Vector2(380,100), Color.White);

                spriteBatch.DrawString(gameFont, MessageOr, new Vector2(600,170), Color.White);
                
                spriteBatch.DrawString(gameFont, MessageEsc, new Vector2(380, 230), Color.White);

                spriteBatch.DrawString(gameFont, MessageScore, new Vector2(380, 400), Color.White);
               
            }
            //Drawing enemie in to the screen
            //spriteBatch.Draw(enemi_Sprite, new Vector2(testenemie.postion.X - testenemie.redius, testenemie.postion.Y - testenemie.redius), Color.White);
            //Go to evet enemies of list and draw
            for (int i = 0; i < handler.enemies.Count; i++)
            {
                //creating a new variable class postion and asigned to be the current enemies
                Vector2 postion = handler.enemies[i].postion;
                //the current redius value
                int redius = handler.enemies[i].redius;
                //draw the sprite at the current enemi postion using postion and redius
                spriteBatch.Draw(enemi_Sprite, new Vector2(postion.X - redius, postion.Y - redius), Color.White);
            }

            spriteBatch.DrawString(timerFont, "Time:" + Math.Floor(handler.timecounter).ToString(), new Vector2(3, 3), Color.White);
            spriteBatch.End();
            base.Draw(gameTime);
        }

        public override void Update(GameTime gameTime)
        {
           // if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
             //   Exit();

            // TODO: Add your update logic here

            //Abenezer starts
            //calling SUpdate() metode from ship class and passing game time and handler
            player.SUpdate(gameTime, handler);
            //calling enemiUpdate() metode from enemi class
            // testenemie.enemiUpdate(gameTime);
            //calling HandlerUpdate() metode from Handler class
            handler.HandlerUpdate(gameTime);

            //if i is lessthan the handler,enemies and incrise
            //go every list in enemies and update them
            for (int i = 0; i < handler.enemies.Count; i++)
            {
                handler.enemies[i].enemiUpdate(gameTime);
                //remove if any enemies are off the screen to the left
                if (handler.enemies[i].postion.X < (0 - handler.enemies[i].redius))
                {

                    handler.enemies[i].offscreen = true;
                }
                //chacking for collison
                int sum = handler.enemies[i].redius + 19; // pic is width 68 px and half is 34 change thisfor pic changs
                //if the distance between two center of the boxes  
                if (Vector2.Distance(handler.enemies[i].postion, player.position) < sum)
                {
                    
                    handler.Ongame = false;
                    //making the ship reapper at the center of the screen
                    player.position = Ships.menuPosition;
                    //exiting out of the loop
                    i = handler.enemies.Count;
                    //clearing every element in the list of enemies
                    handler.enemies.Clear();
                    //volume for the sound
                    //MediaPlayer.Volume += 3f;
                    //Make a sound when colustion
                    Song song = this.game.Content.Load<Song>("Sound/crashSound");
                    MediaPlayer.Play(song);
                    //Replaying the sound again on collison
                    Song backSong = this.game.Content.Load<Song>("Sound/SpaceshipSound");
                    MediaPlayer.IsRepeating = true;
                    MediaPlayer.Play(backSong);
                }
            }
            // iterate through evey item in the enemies list and refer to the current enemies is looking at as 'enemie'
            //if true remove the enemie
            handler.enemies.RemoveAll(enemie => enemie.offscreen);

            base.Update(gameTime);
        }
    }
}
