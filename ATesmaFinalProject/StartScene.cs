﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace ATesmaFinalProject
{
    public class StartScene : GameScene
    {
        private MenuComponent menu;
        public MenuComponent Menu { get => menu; set => menu = value; }

        private SpriteBatch spriteBatch;
        private string[] menuItems = {
            "Start Game",
            "Help",
            "High Score",
            "About",
            "Quit"
        };
        public StartScene(Game game, 
            SpriteBatch spriteBatch) : base(game)
        {
            this.spriteBatch = spriteBatch;
            SpriteFont regularFont = game.Content.Load<SpriteFont>("Fonts/regular");
            SpriteFont hilightFont = game.Content.Load<SpriteFont>("Fonts/hilight");
            

            menu = new MenuComponent(game, spriteBatch, regularFont,
                hilightFont, menuItems);
            this.Components.Add(menu);

        }

        
    }
}
