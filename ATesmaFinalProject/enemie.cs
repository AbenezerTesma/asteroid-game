﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//includind the pakage
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;


namespace ATesmaFinalProject
{
    class enemie 
    {
        public Vector2 postion; //= new Vector2(600, 300);
        public int speed;
        public int redius = 59;
        //if any enemies is offscreen to the left
        public bool offscreen = false;
        //Randomise the y value or the enemies
        static Random r = new Random();

        /// <summary>
        /// constracter for enemie speed
        /// </summary>
        /// <param name="newspeed"></param>
        public enemie(int newspeed)
        {
            speed = newspeed;




            //assign postion for the enemi
            postion = new Vector2(1280 + redius, r.Next(0, 721));//721 is theheight of the screen
        }
        /// <summary>
        ///  the following metode will update the enemi movement 
        /// </summary>
        /// <param name="Time"></param>
        public void enemiUpdate(GameTime Time)
        {
            float t = (float)Time.ElapsedGameTime.TotalSeconds;
            postion.X -= speed * t;
        }
    }
}
