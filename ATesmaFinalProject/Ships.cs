﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//includind the pakage
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
namespace ATesmaFinalProject
{
    class Ships
    {
        //making spped for the ship
        public int speed = 180;

        //seting the menu in the middle
        static public Vector2 menuPosition = new Vector2(640, 360);
        //drowing position for the first field
        public Vector2 position = menuPosition;
        /// <summary>
        ///  the following metode will update the ship movement 
        /// </summary>
        /// <param name="Time"></param>
        public void SUpdate(GameTime Time, Handler handler)
        {
            //Geting the value of what key pressed
            KeyboardState KState = Keyboard.GetState();
            //t = 160th
            float t = (float)Time.ElapsedGameTime.TotalSeconds;
            //only if the user is not on menu 
            if (handler.Ongame)
            { //If the Right key pressed increase x postion to the right
                if (KState.IsKeyDown(Keys.Right) && position.X < 1280)
                {
                    //incrice x postion to the right
                    position.X += speed * t;
                }
                if (KState.IsKeyDown(Keys.Left) && position.X > 0)
                {
                    //decrease X postion to the left
                    position.X -= speed * t;
                }
                if (KState.IsKeyDown(Keys.Down) && position.Y < 720)
                {
                    //incrice Y postion Down
                    position.Y += speed * t;
                }
                if (KState.IsKeyDown(Keys.Up) && position.Y > 0)
                {
                    //decrease Y postion Down
                    position.Y -= speed * t;
                }
            }
        }
    }
}
